package com.silvioapps.drawing;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;

public class FingerLine extends View {
    private final Paint mPaint;
    private SparseArray<Float> startXList = new SparseArray<>();
    private SparseArray<Float> startYList = new SparseArray<>();
    private SparseArray<Float> endXList = new SparseArray<>();
    private SparseArray<Float> endYList = new SparseArray<>();
    private int line = -1;

    public FingerLine(Context context) {
        this(context, null);
    }

    public FingerLine(Context context, AttributeSet attrs) {
        super(context, attrs);
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setStyle(Style.STROKE);
        mPaint.setColor(Color.RED);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        for(int i=0;i<startXList.size();i++){
            float startX = startXList.get(i);
            float startY = startYList.get(i);
            float endX = endXList.get(i);
            float endY = endYList.get(i);

            canvas.drawLine(startX, startY, endX, endY, mPaint);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                line++;
                startXList.put(line, event.getX());
                startYList.put(line, event.getY());
                endXList.put(line, event.getX());
                endYList.put(line, event.getY());
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                endXList.put(line, event.getX());
                endYList.put(line, event.getY());
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                endXList.put(line, event.getX());
                endYList.put(line, event.getY());
                invalidate();
                break;
        }
        return true;
    }
}